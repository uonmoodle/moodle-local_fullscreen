﻿ABOUT
==========
The 'Fullscreen toggle button' local plugin was developed by
    Barry Oosthuizen - barry.oosthuizen@nottingham.ac.uk

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The 'Fullscreen toggle button' plugin allows a user to toggle fullscreen mode on and off.

This plugin has been tested with the Boost and Classic theme.

* In the Boost theme the content of limited width pages will change to expand the full width of the page.
* In the Classic theme this will hide the block areas and expand allow the content areas to expand to the
  full width of the page.

The current screen mode is stored in user preferences which means the screen mode will persist until it is toggled again.

The button appears on all pages except "My home", login, embedded, popup, frametop, redirect and base page layouts.

Full screen mode can be toggled using the keyboard using Ctrl + Alt + b by default,
the system administrator can configure the keyboard combination in the plugins settings.

INSTALLATION
==========
The 'Fullscreen toggle button' local plugin follows the standard installation procedure.

1. Create folder <path to your moodle dir>/local/fullscreen.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.
